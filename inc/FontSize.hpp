/**
 * @file FontSize.hpp
 * @author Julian Neundorf
 * @brief
 * @version 0.1
 * @date 2024-04-20
 *
 * Remarks:
*/
#pragma once

#include <utility>
#include <cstdint>

namespace Graphics {
  class FontSize {
  public:
    enum FS {
      /* Stored fonts */
      S8 = 0,
      S12 = 1,
      S16 = 2,
      S20 = 3,
      S24 = 4,

      /* Automatically scaled fonts */
      G36 = 5,
      G48 = 6,
      G60 = 7,
      G72 = 8,
      G84 = 9,
      G96 = 10,
      MAX // DO NOT USE
    };

    operator std::uint16_t() const { return fontSize_; }

    FontSize& operator--(int) {
      return *this = *this - 1;
    }

    FontSize& operator++(int) {
      return *this = *this + 1;
    }

    FontSize operator+=(const std::int32_t right) {
      return *this = *this + right;
    }

    FontSize operator-=(const std::int32_t right) {
      return *this = *this - right;
    }

    FontSize operator-(const std::int32_t right) {
      return *this = *this + -right;
    }

    FontSize operator+(const std::int32_t right) {
      const auto fs = static_cast<std::underlying_type<FS>::type>(fontSize_);
      if (right >= 0) {
        if (fs + right < static_cast<std::underlying_type<FS>::type>(MAX)) {
          fontSize_ = static_cast<FS>(fs + right);
        } else {
          fontSize_ = static_cast<FS>(static_cast<std::underlying_type<FS>::type>(MAX) - 1);
        }
      } else if (right < 0) {
        if (static_cast<std::uint16_t>(fs) > -right) {
          fontSize_ = static_cast<FS>(fs + right);
        } else {
          fontSize_ = static_cast<FS>(0);
        }
      }

      return *this;
    }

    FontSize& operator=(const FontSize::FS& other) {
      fontSize_ = other;
      return *this;
    }

    bool operator==(const FontSize& rhs) {
      return this->fontSize_ == rhs.fontSize_;
    }

    FontSize() : fontSize_(S8) {}
    FontSize(FS fs) : fontSize_(fs) {}

    std::uint8_t toUint() const {
      return static_cast<std::uint8_t>(fontSize_);
    }

    FS fontSize_;
  };
};