/**
 * @file Fonts.hpp
 * @author Julian Neundorf (julian9dorf@gmail.com)
 * @brief Delivers the fonts in multiple sizes
 * @version 2.0
 * @date 2022-05-08
 *
 * Remarks:
*/
#pragma once
#include <cstdint>
#include <stdio.h>
#include <memory>
#include <vector>

#include "FontSize.hpp"

namespace Graphics {
  constexpr std::uint8_t NumberOfFonts = 11; // Number of fonts
  constexpr std::uint8_t MaxStoredFont = 4; // Biggest font which is stored

  /* Internal font-class */
  class Font {
  public:
    Font(std::uint8_t height, std::uint8_t width, FontSize size);
    const std::uint16_t Height;
    const std::uint16_t Width;
    const FontSize fontSize;
    const Font* bigger;
    const Font* smaller;
    const Font* Smaller() { return smaller != NULL ? smaller : this; }
    const Font* Bigger() { return bigger != NULL ? bigger : this; }
    std::unique_ptr<std::uint8_t[]> getFont(char c);
    const std::uint8_t* getFontData(std::uint8_t c) const;

  private:
  };

  /* Internal font-list-class */
  class Fonts {
  public:
    static Fonts& getInstance();
    Font& getFont(FontSize fontSize) const;
    std::uint8_t getHeight(FontSize fontSize) const;
    std::uint8_t getWidth(FontSize fontSize, char c) const;
    std::uint16_t getWidth(FontSize fontSize, std::vector<char>& c) const;
    static Font& GetFont(FontSize fontSize);
    static std::uint8_t GetHeight(FontSize fontSize);
    static std::uint8_t GetWidth(FontSize fontSize, char c);
    static std::uint16_t GetWidth(FontSize fontSize, std::vector<char>& c);

    static void ReplaceSpecialChars(std::vector<char>& str);

  private:
    Fonts();

    Font* list_[NumberOfFonts];
  };

  /* External font-class */
  class cFont {
  public:
    const std::uint8_t* Table;
    std::uint16_t Width;
    std::uint16_t Height;
  };
};
